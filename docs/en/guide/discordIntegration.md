---
title: Discord integration
tags: [
  integration,
  discord,
  share,
  delete
]
lang: en
---

# Discord integration

The discord integration page allows you to manage the bot in a discord server.

# Options

<p class="step">Channel</p>
<img class="image-smaller" :src="$withBase('/screenshot-discord-integration-channel-2020-04-10.png')" alt="Channel">

Select the channel you want to use for the Discord bot.

<p class="step">Pin message</p>
<img class="image-smaller" :src="$withBase('/screenshot-discord-integration-pin-message-2020-04-10.png')" alt="Pin message">

Select whether or not you want the message pin.

::: tip Tip
A message pin is displayed on the left at the top of the game screen and is visible to all players of the game for a certain number of times.
:::

<p class="step">Historique</p>
<img class="image-smaller" :src="$withBase('/screenshot-discord-integration-historique-2020-04-10.png')" alt="Historique">

Select whether or not you want to keep the history.

# Supprimer l'intégration discord
<br>
<img :src="$withBase('/screenshot-en-discord-integration-supprimer-integration-2020-04-10.png')" alt="Supprimer l'intégration">

You can delete the Discord integration by clicking on the “Delete integration” button in red.
 
::: tip Tip
It will be possible to reactivate the integration at any time.
:::

