---
title: Server selection
tags: [
  selection,
  servers,
  add,
  download,
  client,
  local
]
lang: en
---

# Server selection

## Download the local client
<br/>
<img class="image-bigger" :src="$withBase('/screenshot-en-client-locale-2020-04-10.png')" alt="Local client">

To download the local client, click on "here".

::: tip Tip
A local client will be used to link the information between our database and your infrastructure. You should install the client on the machine where you want to start the server. Windowx, Linux and Mac executables will be available to you. <br> </br>
Click here to view the documentation for [download local client](https://gitlab.com/odeyr/service-dotnet/-/releases).
:::

After installing the local client, it will probably be necessary to restart the computer to apply the changes.

## Add server
<p class="step">1. Click on the add button</p>
<br/>
<img class="image-smaller" :src="$withBase('/screenshot-en-add-server-2020-04-10.png')" alt="Add">

<p class="step">2. Enter the server name and then click on the "Next" button</p>
<br/>
<img class="image" :src="$withBase('/screenshot-en-add-server-name-2020-04-10.png')" alt="Server name">

Choose a name for your server. This name will be requested on the Minecraft app when you add the server to the game.

<p class="step">3. Select the type of server and then click on the "Next" button</p>
<br/>
<img class="image" :src="$withBase('/screenshot-en-add-server-type-2020-04-10.png')" alt="Server type">

Choose the server category that suits you. The option currently being considered is Vanilla.

::: tip Tip
Vanilla means that this is a classic version of the server. Later, a moderated version will also be available for selection.

CraftBukkit is a modification of Minecraft's server software(Vanilla Minecraft). Much more optimized for large scale deployment than Vanilla. For more information, visit https://getbukkit.org/ .
:::

After installing the local client, it will probably be necessary to restart the computer to apply the changes.

<p class="step">4. Select the Minecraft version of the server and then click on the "Next" button</p>
<br/>
<img class="image" :src="$withBase('/screenshot-en-add-server-version-2020-04-10.png')" alt="Server version">

Choose the Minecraft version of the server. Some versions are supported as long as others are not supported on the Odeyr application.

::: tip Tip
The currently supported versions are: 1.15.2, 1.12.2 and 1.7.10. Other versions will be coming soon.
:::

<p class="step">A confirmation following the creation of the server will be displayed:</p>
<img class="image" :src="$withBase('/screenshot-en-add-server-confirmation-2020-04-10.png')" alt="Confirmation">

It will be important install first the local client on your machine to install on your machine.