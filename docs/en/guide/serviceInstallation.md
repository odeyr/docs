---
title: Service installation
tags: [
  installation,
  service,
  software,
  clone,
  commands
]
lang: en
---

# Service installation

## How to install the service
<br/>
Here is a short video for the installation of the service. Unfortunately, the video is only available in French.
<br/>
<br/>
<video src="/service-installation-2020-05-11.mp4" width="750" height="450" controls preload></video>

## Required software
Install Visual Studio with these workloads: .NET desktop development, Universal Windows Platform development et .NET Core cross-platform development. A 64-bit version of Java also needs to be instaled

## Download the service
Get the project from the latest release [here](https://gitlab.com/odeyr/service-dotnet/-/releases)

## How to install (Text guide)
Add a new User environment variable with ODEYR_DATA_LOCATION as the variable name and the directory to where you wish your server to be installed to as the variable value.
[Guide on how to create a new environment variable](https://www.twilio.com/blog/2017/01/how-to-set-environment-variables.html)
<br/>
<br/>
<img class="image" src="/screenshot-add-environment-variable.png">

Inside the directory you’ve set as the environment variable value, create a new file called “odeyr-global.properties” with these three fields: email, password, authenticated. The email and password field must be filled with your Odeyr crendentials. But the authenticated field should be left empty or set to false.
<br/>
<br/>
<img class="image" src="/screenshot-odeyr-global-properties.png">

Restart your PC for the changes with the environment variable to take effect and you're good to go!