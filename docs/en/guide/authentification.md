---
title: Authentication
tags: [
  authentication,
  connection,
  password,
  inscription,
  registration,
  forgot,
  email
]
lang: en
---

# Authentication

## Connection
<br/>
<img class="image" :src="$withBase('/screenshot-en-connexion-2020-04-10.png')" alt="Connection">

The login page allows you to identify yourself with the email and password entered during registration.

## Registration
<br/>
<img class="image" :src="$withBase('/screenshot-en-register-2020-04-10.png')" alt="Registration">

The registration page allows you to register with a username, email and password.

## Forgot password
<br/>
<img class="image" :src="$withBase('/screenshot-en-forgot-password-2020-04-10.png')" alt="Forgot password">

The forgotten password page allows you to reset your password. An email will be sent to you shortly, in order to obtain the link to reset the password.

<p class="step">Received email</p>

Once in your mailbox, click on the "Reset password" button. If the button does not work, click on the link. You will then be redirected to the site in order to reset your password. You will then be able to log in again with your new password.