---
title: My servers
tags: [
  servers,
  status,
  modify,
  edit,
  delete
]
lang: en
---

# My servers

The My Servers page is located in the drop-down menu at the top right of the navigation window. It allows you to delete, modify and install a server.

## Server status

The status is displayed as an icon which changes color depending on the status of the server as on the page [server control](/en/guide/serverControl).

<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Server down"></div>
  <div class="item"><p class="step">Server down</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-running-2020-4-20.png')" alt="Server start"></div>
  <div class="item"><p class="step">Server start</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-loading-2020-4-20.png')" alt="Server loading"></div>
  <div class="item"><p class="step">Server loading</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Server with an error"></div>
  <div class="item"><p class="step">Server with an error</p></div>
</div>

## Server options
<br>
<img :src="$withBase('/screenshot-en-my-servers-menu-2020-04-10.png')" alt="Menu">

To display the server options, click on the icon with the three vertical dots.

### Modify
To modify the server, click on "Modify", which will lead to the page [edit server](/en/guide/editServer).

### Delete
To delete the server, click on “Delete”. Confirmation will be requested before removing the server.

::: danger Remove server
It will no longer be possible to restore the same server once deleted. All data on the server will be deleted.
:::