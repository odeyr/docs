---
title: Server preferences
tags: [
  preferences,
  server,
  advanced,
  options
]
lang: en
---

# Server preferences

The server preferences page allows you to modify the server options.

## Normal options

<p class="step">Difficulty</p>

Refer to the Minecraft wiki by clicking on this link to learn more about the difficulties.

<p class="step">Game mode</p>

Refer to the Minecraft wiki by clicking on this link to learn more about the game modes.

<p class="step">Maximum world height</p>

Enter a number greater than 0 for the maximum world height by entering the number.

<p class="step">Welcome word</p>

Enter the welcome word that all players on the server will see on the Minecraft game.

<p class="step">Define the resource pack</p>

Refer to the Minecraft wiki by clicking on this link to learn more about resource packs.

<p class="step">Viewing distance</p>

Enter a number between 1 and 32 for the viewing distance.

<p class="step">Force game mode</p>

Refer to the Minecraft wiki by clicking on this link to learn more about the game modes.

<p class="step">Safe mode</p>

Select whether or not you want a safe mode.

<p class="step">Maximum number of players</p>

Enter a number greater than 0 for the maximum number of players.

<p class="step">Allow PVP</p>

Refer to the Minecraft wiki by clicking on this link to learn more about PVP.


## Advanced options
<br>
<img :src="$withBase('/screenshot-en-serveur-preferences-avancee-2020-04-10.png')" alt="Advanced options">

Click on the "Advanced Options" bar at the bottom of the page to see the advanced options.

<p class="step">Allow to fly</p>

Select whether or not you want players to be able to fly.

Refer to the Minecraft wiki by clicking on this link to learn more about flying.

<p class="step">Allow hell</p>

Refer to the Minecraft wiki by clicking on this link to learn more about hell.

<p class="step">Allow command blocks</p>

Refer to the Minecraft wiki by clicking on this link to learn more about command blocks.

<p class="step">Use GameSpy4</p>

The GameSpy4 is used to obtain information from the server.

<p class="step">Define the port for GameSpy4</p>

Enter a number greater than 1 to define the port for GameSpy4.

<p class="step">Use remote access</p>

Remote access is the connection to the remote server.

<p class="step">Define the port for remote access</p>

Enter a number greater than 1 to define the port for remote access.

<p class="step">Level of permission to functions</p>

Enter a number between 1 and 4 for the function permission level.

<p class="step">Generate structures</p>

Select whether or not you want to generate structures.

<p class="step"> Name of the world</p>

Enter the name of the world.

<p class="step">Seed of the world</p>

Refer to the Minecraft wiki by clicking on this link to learn more about the seeds.

<p class="step">Type of the world</p>

Refer to the Minecraft wiki by clicking on this link to learn more about the types of world.
<p class="step">Maximum number of ticks</p>

Enter a number greater than 0 for the maximum number of ticks.
Refer to the Minecraft wiki by clicking on this link to learn more about ticks.

<p class="step">Maximum world size</p>

Enter a number greater than 0 for the maximum world size

<p class="step">Packet compression size </p>

Enter a number greater than -1 for the packet compression size.
Refer to the Minecraft wiki by clicking on this link to learn more about packages.

<p class="step">Verifying Mojang accounts </p>

Select whether or not you want Mojang account verification.

<p class="step">Default permission from the OP</p>

Select the operator's default level.
Refer to the Minecraft wiki by clicking on this link to learn more about the operator.
<p class="step">AFK time before disconnection</p>

Enter a number greater than 0 for the AFK time before disconnection.

<p class="step">The server port </p>

Enter a number greater than 1 for the server port.
Refer to the Minecraft wiki by clicking on this link to learn more about servers.

<p class="step">The server IP address </p>

Enter the IP address of the server.

<p class="step"> Sending snoop data </p>

Refer to the Minecraft wiki by clicking on this link to learn more about snoop.

<p class="step">Appearance of animals </p>

Select whether or not you want the appearance of animals.

<p class="step">Non-player character appearances (NPCs) </p>

Refer to the Minecraft wiki by clicking on this link to learn more about non-player characters.

<p class="step">Appearance of monsters </p>

Refer to the Minecraft wiki by clicking on this link to learn more about creatures.

<p class="step">Optimization for Linux server </p>

Select whether or not you want to optimize for Linux server.

<p class="step">Size of the protection zone </p>

Refer to the Minecraft wiki by clicking on this link to learn more about the protection zone.

<p class="step">Oblige the use of the "White list" </p>

Refer to the Minecraft wiki by clicking on this link to learn more about the "White list".

<p class="step">Use the "White list" </p>

Refer to the Minecraft wiki by clicking on this link to learn more about the "White list".
