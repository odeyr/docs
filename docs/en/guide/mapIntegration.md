---
title: Map integration
tags: [
  map,
  integration,
]
lang: en
---

# Map intégration

The map allows you to see the game map. You can also see the players who are playing at the same time.

To view the page, you must have a "CraftBukkit" server.

<img class="icon" :src="$withBase('/screenshot-map-integration-2020-05-11.png')" alt="Map integration">

There is also a 3D map.

<img class="icon" :src="$withBase('/screenshot-map-3d-integration-2020-05-11.png')" alt="3D map integration">
