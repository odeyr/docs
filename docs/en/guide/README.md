---
title: Introduction
tags: [
  projet,
  presentation,
  start,
  introduction
]
lang: en
---

::: warning Documentation in construction
:construction:
The documentation is being written, it may be that the information is not 100% up to date and changes regularly, that some page does not contain all the information and that there are some typing errors or mistakes.
:::

# Introduction

The Odeyr Project is an initiative to simplify the installation, sharing and access to your personal game servers. What is meant by personal is a server that runs on your own computer and therefore uses the resources of your computer.

Traditionally, there are two ways to have a game server to play with friends. The first requires paying a specialized host, which can be expensive. The second is that ODEYR makes home hosting easy and quick to configure.

## How does it work

::: tip Technical functioning
The next paragraph does not address the technical approach of the project, but rather the basic principles that govern the application from the point of view of a user.

If you are looking for technical documentation, go to the [developer documentation section](/dev).
:::
