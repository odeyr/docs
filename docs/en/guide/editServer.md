---
title: Edit server
tags: [
  modify,
  edit,
  server,
  options
]
lang: en
---

# Edit serveur

## Edit data

The graph represents the number of players in real time who are connected to the server. On the x-axis of the graph is the number of players.

<p class="step">Edit server name</p>
<img class="icon" :src="$withBase('/screenshot-en-edit-server-edit-2020-4-20.png')" alt="Edit server name">
<br>
<p class="step">1. Click on the pencil in the gray button</p>
<img class="delete-icon" :src="$withBase('/screenshot-edit-server-create-bouton-2020-4-20.png')" alt="Edit">

<p class="step">2. Enter the new server name</p>

<p class="step">3. Click on the floppy in the green button</p>
<img class="delete-icon" :src="$withBase('/screenshot-edit-server-save-bouton-2020-4-20.png')" alt="Save modifications">

<p class="step">4. Wait until it finishes loading</p>
<img class="delete-icon" :src="$withBase('/screenshot-edit-server-loading-bouton-2020-4-20.png')" alt="Loading">

## Remove server
<br>
<img class="icon" :src="$withBase('/screenshot-en-edit-server-remove-server-2020-4-20.png')" alt="Remove server">
<br>

You can delete the server by clicking on the "Delete server" button in red. Confirmation will be requested before removing the server.
 
::: danger Warning
It will not be possible to reactivate the server. You will lose everything.
:::


