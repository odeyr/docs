---
title: List of players
tags: [
  List,
  players,
  op
  banned,
  ban
  kick
]
lang: en
---

# List of players
The player list page contains the list of players who have connected to the server at least once.

<p class="step">Carte du joueur</p>

The player account photo, player username and action buttons are displayed.

<p class="step">Op</p>
<img class="icon" :src="$withBase('/screenshot-players-list-verified-2020-04-10.png')" alt="OP">

Click on the icon to make a user operator.

::: tip Tip
An OP user is a user who has rights that some users do not have. In other words, he is an administrator.
:::

<p class="step">Ban</p>
<img class="icon" :src="$withBase('/screenshot-players-list-block-2020-04-10.png')" alt="Ban">

Click on the icon to ban a user.

::: tip Tip
A banned user is a user who can no longer access the server.
:::

<p class="step">Kick</p>
<img class="icon" :src="$withBase('/screenshot-players-list-warning-2020-04-10.png')" alt="Kick">

Click on the icon to kick a user.

::: tip Tip
A kicked user is a user who will be rejected from the current game, but the user will be able to reconnect to re-enter the server.
:::