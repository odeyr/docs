---
title: Profil
tags: [
  profil,
  username,
  password,
  change password,
  modify,
  edit,
  modification
]
lang: en
---

# Profil

The profile page contains all the information on the profile of the person connected.

<img class="image" :src="$withBase('/screenshot-en-profile-form-2020-04-10.png')" alt="Profil">

## Edit data
<br>
<img class="image" :src="$withBase('/screenshot-en-profile-modifier-2020-04-10.png')" alt="Edit data">

To activate data modification, click on the "Modify" button at the bottom right.

It is possible to change the user name, email and password. If you change the password, you will have to confirm it in "Confirm password".

When the modifications are finished, click on the button "Submit" in green.