---
title: Telemetry
tags: [
  telemetry,
  ping,
  number of players,
  players,
  graphic
]
lang: en
---

# Telemetry

## Ping

The ping graph lets you know in real time the connectivity of the server.

<img class="icon" :src="$withBase('/screenshot-telemetry-ping-2020-4-20.png')" alt="Ping">

::: tip Tip
The higher it is, the more latency there is. In other words, the longer the transmission delay in communications.
:::

## Numbers of players

The graph of the number of players allows you to know in real time the number of players who are connected to the server. On the x-axis of the graph is the number of players.

<img class="icon" :src="$withBase('/screenshot-telemetry-players-2020-4-20.png')" alt="Number of players">