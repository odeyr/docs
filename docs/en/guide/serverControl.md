---
title: Server control
tags: [
  control,
  server,
  status,
  terminal,
  console
]
lang: en
---

# Server control
The server control page allows you to manage the status of the server and view the console.

## Server information
The icon on the left allows you to see the status of the server. If you place the mouse on the circle, the status will be written.

<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Server down"></div>
  <div class="item"><p class="step">Server down</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-running-2020-4-20.png')" alt="Server start"></div>
  <div class="item"><p class="step">Server start</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-loading-2020-4-20.png')" alt="Server loading"></div>
  <div class="item"><p class="step">Server loading</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Server with an error"></div>
  <div class="item"><p class="step">Server with an error</p></div>
</div>

## Start or stop server
Click the button to change the status of the server.

If the server is started, there will be a "Stop" button in red to stop the server.

<img class="image-bigger" :src="$withBase('/screenshot-en-server-controls-server-running-2020-4-20.png')" alt="Server down">

If the server is stopped, there will be a green "Start" button to start the server.

<img class="image-bigger" :src="$withBase('/screenshot-en-server-controls-server-start-2020-4-20.png')" alt="Server start">

## Console
The console displays all the logs coming out of the server and provides a quick overview of the commands that have been executed. It will also be possible to send Minecraft commands through the browser console.

<img class="image-bigger" :src="$withBase('/screenshot-en-server-controls-console-2020-4-20.png')" alt="Server start">