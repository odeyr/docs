---
title: Installation of the Service/Client for development
tags: [
  installation,
  service,
  software,
  clone,
  commands
]
lang: en
---

# Service installation for development

## Required software
Install Visual Studio with these workloads: .NET desktop development, Universal Windows Platform development et .NET Core cross-platform development. A 64-bit version of Java also needs to be installed

## Items to clone
Clone the project [ici](https://gitlab.com/odeyr/service-dotnet/-/tree/develop).

## Setup the project
First off you will need an sshtunnel.pem file, if you don't have one, you can find one inside the latest release of the [Odeyr Service](https://gitlab.com/odeyr/service-dotnet/-/releases). Add your sshtunnel.pem file to the following directory inside the project folder: service-dotnet\OdeyrWorkerService\

Add a new User environment variable with ODEYR_DATA_LOCATION as the variable name and the directory to where you wish your server to be installed to as the variable value.
[Guide on how to create a new environment variable](https://www.twilio.com/blog/2017/01/how-to-set-environment-variables.html)
<br/>
<br/>
<img class="image" src="/screenshot-add-environment-variable.png">

Inside the directory you’ve set as the environment variable value, create a new file called “odeyr-global.properties” with these three fields: email, password, authenticated. The email and password field must be filled with your Odeyr crendentials. But the authenticated field should be left empty or set to false. An optional field can also be added to this file, the optional field name is 'endpoint'. This field value allows to specify to which endpoint you wish to connect to. For example http://localhost:4000 could be set as the 'endpoint' field value.
<br/>
<br/>
<img class="image" src="/screenshot-odeyr-global-properties.png">

Restart your PC for the changes with the environment variable to take effect and you're good to go!