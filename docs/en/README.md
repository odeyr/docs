---
home: true
heroImage: https://cdn.odeyr.org/logo/logo-high-2048.png
heroText: The Odeyr Project
tagline: The right way to manage your servers
actionText: User Guide →
actionLink: /guide/
features:
- title: Simplicity First
  details: Installing, sharing and managing a self-hosted game server will never be easier.
- title: Reliable
  details: We work hard ot make sure our infrastructure is always up and running.
- title: Performant
  details: We are using the lastest technologies availables to providee a fluid and simple user experience.
footer: GPLv3 Licensed | Copyright © 2020-present Odeyr
---
