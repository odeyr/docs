module.exports = {
  title: 'Odeyr Documentation',
  description: 'Documentation of the free self-hosted game server manager',
  head: [
    [
      'link',
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: 'https://cdn.odeyr.org/favicons/apple-touch-icon.png',
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: 'https://cdn.odeyr.org/favicons/favicon-32x32.png',
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: 'https://cdn.odeyr.org/favicons/favicon-16x16.png',
      },
    ],
    [
      'link',
      {
        rel: 'mask-icon',
        href: 'https://cdn.odeyr.org/favicons/safari-pinned-tab.svg',
        color: '#625370',
      },
    ],
    [
      'link',
      {
        rel: 'shortcut icon',
        href: 'https://cdn.odeyr.org/favicons/favicon.ico',
      },
    ],
    ['meta', { name: 'msapplication-TileColor', content: '#625370' }],
    [
      'meta',
      {
        name: 'msapplication-config',
        content: 'https://cdn.odeyr.org/favicons/browserconfig.xml',
      },
    ],
    [
      'meta',
      {
        name: 'og:image',
        content: 'https://cdn.odeyr.org/logo/logo-high-2048-text-right-en.png',
      },
    ],
  ],
  locales: {
    '/en/': {
      lang: 'en',
      title: 'Odeyr Documentation',
      description: 'Documentation of the free self-hosted game server manager',
    },
    '/fr/': {
      lang: 'fr',
      title: 'Documentation Odeyr',
      description:
        'Documentation du gestionnaire gratuit de serveurs de jeux personnels',
    },
  },
  themeConfig: {
    logo: 'https://cdn.odeyr.org/logo/logo-low-2048.png',
    repo: 'https://gitlab.com/odeyr',
    docsRepo: 'https://gitlab.com/odeyr/docs',
    docsDir: 'docs',
    docsBranch: 'develop',
    editLinks: true,

    locales: {
      '/en/': {
        selectText: 'Languages',
        label: 'English',
        ariaLabel: 'Languages',
        searchPlaceholder: 'Search...',
        editLinkText: 'Help us improve this page!',

        nav: [
          { text: 'Home', link: '/en/' },
          { text: 'User Guide', link: '/en/guide/' },
          { text: 'Dev Docs', link: '/en/dev/' },
          { text: 'Patreon', link: 'https://patreon.com/odeyr' },
        ],
        sidebar: {
          '/en/guide/': getGuideSidebar('Guide'),
        },
      },
      '/fr/': {
        selectText: 'Langages',
        label: 'Français',
        ariaLabel: 'Langages',
        searchPlaceholder: 'Rechercher...',
        editLinkText: 'Aidez-nous à améliorer cette page!',

        nav: [
          { text: 'Accueil', link: '/fr/' },
          { text: 'Guide Utilisateur', link: '/fr/guide/' },
          { text: 'Documentation Développeur', link: '/fr/dev/' },
          { text: 'Patreon', link: 'https://patreon.com/odeyr' }
        ],
        sidebar: {
          '/fr/guide/': getGuideSidebar('Guide'),
          '/fr/dev/': getDevSidebar('Dévelopement'),
        },
      },
    },
  },
  plugins: [
    [
      'vuepress-plugin-redirect',
      {
        // provide i18n redirection
        // it will automatically redirect `/foo/bar/` to `/:locale/foo/bar/` if exists
        locales: true,
      },
    ],
  ],
}

function getDevSidebar(groupA) {
  return [
    {
      title: groupA,
      collapsable: false,
      children: [
        '',
        'installation/api',
        'installation/dashboard',
        'installation/service',
        'architecture',
      ],
    },
  ]
}

function getGuideSidebar(groupA) {
  return [
    {
      title: groupA,
      collapsable: false,
      children: [
        '',
        'serviceInstallation',
        'authentification',
        'serverSelection',
        'serverControl',
        'telemetry',
        'playersList',
        'serverPreferences',
        'mapIntegration',
        'profile',
        'myServers',
        'editServer'
      ]
    }
  ]
}
