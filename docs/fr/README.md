---
home: true
heroImage: https://cdn.odeyr.org/logo/logo-high-2048.png
heroText: Le Projet Odeyr
tagline: La bonne façon de gérer vos serveurs
actionText: Guide Utilisateur →
actionLink: /guide/
features:
- title: Simplicité
  details: Installer, partager et gérer un serveur de jeu personnel ne sera jamais aussi facile.
- title: Fiable
  details: Nous travaillons fort pour nous assurer que notre infrastructure est toujours en fonction.
- title: Performance
  details: Nous utilisons les plus récentes technologiques afin de fournir une expérience fluide et simple.
footer: Sous license GPLv3 | Droit d'auteurs © 2020-présent Odeyr
---
