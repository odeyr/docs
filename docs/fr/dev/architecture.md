---
title: Architecture
lang: fr
tags: { fonctionnement, comment-ca marche, explication }
---

# Architecture

Les problèmes que la plateforme Odeyr tente de résoudre sont assez complexes. C'est pourquoi il a été nécessaire de refléchir à une solution robuste et relativement simple.

[[toc]]

## Architecture Logiciel

Il a y deux composants logiciels qui permettent de faire fonctionner la plateforme. Sans eux, Odeyr ne serait rien.

Premièrement, il y a le service qui s'éxécute sur la machine de l'utilisateur. C'est grâce à se service que nous sommes en messure de controller les serveurs sur l'ordinateur personnel de l'utilisateur. Il n'est tout simplement pas possible qu'une application web s'attache à un processus comme le service le fait, le navigateur web agit comme une boite à sable.

Deuxièmement, il y a GraphQL ou plus précisement, le serveur de websocket. Afin d'envoyer une commande sur un des serveurs de l'utilisateur le service à besoin de recevoir cette information, il existe deux solutions pour cela, du [polling](<https://en.wikipedia.org/wiki/Polling_(computer_science)>) ou des [sockets](https://en.wikipedia.org/wiki/Network_socket). Le dernier étant beaucoup plus performant que le premier, GraphQL nous permet de créé un serveur de websocket avec simplicité: à chaque changement sur la base de données, ces derniers sont pousser dans les websockets et il ne suffit que d'écouter.

Voici un schéma qui décrit les interactions entre les différents composants de l'architecture.  
![Odeyr Component Diagram](/odeyr-component-diagram.png)

### Processus de création d'un serveur

En utilisant la plateforme Odeyr, lorsqu'un utilisateur créé un serveur, deux ports lui sont assigné. Le permier pour le jeux, et le deuxième pour Rcon.

Ensuite, le service recoit qu'un nouveau serveur à été créé pour l'utilisateur, ce dernier entreprend donc la tâche de télécharger l'éxécutable du serveur pour la version sélectionner.

Lors du premier démarage du serveur, le EULA se fait accepter automatiquement. Par la suite, le serveur démarre normalement.

Lorsque le serveur à terminé de démarer, un tunnel SSH est établie entre le serveur d'Odeyr et le serveur en utilisant les ports assignés précédamment. Dans le _bundle_ du service, une clé privé SSH est founi afin de se connecter sur le compte permettant de faire une telle opération. Ce compte n'a aucun accès sur le serveur et n'a même pas accès à un terminal de ligne de commande.

## Architecture Matériel

Au moment de l'écriture de cette documentation la plateforme Odeyr utilise:

- CDN (Bucket S3 et CloudFront) pour les images
- Netlify pour l'hébergement des differents sites
  - Landing Page
  - Dashboard
  - Version de dévelopment du Dashboard
  - Documentation
- Serveur VPS pour l'hébergement de l'API

L'utilisation d'un CDN permet aux images des différents sites d'être envoyer plus rapidement aux clients web ce qui diminue le temps de chargement des sites. Il est possible d'utiliser la même architecture que le CDN pour héberger les sites mais Netlify offre certaines fonctionnalités qui rendent l'expérience de dévelopement beaucoup plus simple et c'est pourquoi nous les avons choisi.

Voici le schéma présenté plus haut avec les différents nom de domaine utilisés pour chacun des composants.  
![Odeyr Components with Names](/odeyr-components-diagram-with-names.png)

### Déployements

Vous aurez peut-être remarquer que le code la plateforme Odeyr se trouve sur GitLab. Ce n'est pas pour rien, nous voulions avoir accès à une pipeline CI/CD facilement et à aucun coût. De plus, nous avions déjà de l'expérience avec cette technologie.

Les déployements sont assez simple, nous utilisons une image qui permet d'avoir un accès simple et rapide au terminal ssh. Ainsi, il est très facile d'éxécuté des commandes vers et sur le serveur de l'API. Le deployement en tant que tel est principalement géré par PM2, ce qui simplifie énormément la tâche. Les différents site se font aussi déployé automatiquement par Netlify et s'intégre très bien avec le processus du Git-Flow.

### Architecture interne du VPS

Nous utilisons docker afin de faire fonctionner Prisma, la base de données et Traefik puis PM2 pour gérer les instances de l'API. L'API ne fonctionne pas officiellement avec docker directement étant donnée que pour le moment ce n'est pas une priorité et PM2 permet de simplifier énormément le processus de déployement.

L'API est déservit sous HTTPS, pour y parvenir nous utilisons Traefik qui permet d'automatiser la génération des certificats et pourra servir de load-balancer entre les instances de l'API dans le futur.

À des fins de sécurité, tous les ports de la machine sont fermés, excluant le port ssh, http et https bien entendu. De plus, afin d'être en messure de fournir une adresse unique et permante pour chaque serveur, les ports allant de 49152 à 65535 sotn destiné à être utilisé pour les serveurs. Une meilleur solution pourrais être à envisagé étant donnée que nous ouvront 2 ports (le jeux + rcon) pour chaque serveur créé sur la plateforme, il y a une limite de 8191 serveurs. Si nous atteignont cette limite, la seule solution pour le moment est de louer un nouveau serveur et assigner les nouveaux serveurs de jeux sur ce dernier.
