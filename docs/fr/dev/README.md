---
title: Introduction
lang: fr
tags: { intro, developement, commencement, début }
---

# Introduction

::: warning Section technique
Cette section s'addresse à la partie technique d'Odeyr et explique les concepts derrière le fonctionnement du système afin d'aider les futurs développeurs à comprendre rapidement.

Si vous êtes à la recherche de la partie non technique, par exemple si vous chercher à savoir comment utiliser l'application, rendez-vous dans le [guide utilisateur](/fr/guide).
:::

## Comment cela fonctionne

Odeyr est composé de plusieurs parties, chacune d'elle assure le bon fonctionnement de l'écosystème et y est essentiel. Les composants majeures de cet écosystème sont décris ci-dessous. Chacun des projets à aussi droit à son propre répertoire sur GitLab.

Pour comprendre le fonctionnement inter-composant, veuillez vous référer à la page concernant l'[archictecture](/fr/dev/architecture).

### API

> [https://gitlab.com/odeyr/api](https://gitlab.com/odeyr/api)

L'API est le coeur de la plateforme. Il permet de faire le lien entre le dashbaord et le client logiciel ainsi que de gérer la base de données.

Nous avons choisi de développer l'API en [Node.js](https://nodejs.org) et [TypeScript](https://www.typescriptlang.org/) en utilisant le standard [GraphQL](https://graphql.org). Sous le capot, il y a une base de donnée, celle-ci est une base de données relationnelle [MySQL](https://www.mysql.com/) et nous utilisons [Prisma](https://prisma.io) afin de la gérer et communiquer avec.

Prochainement, c'est aussi le rôle de l'API de gérer le robot Discord. Cette partie est encore en cours de développement.

[Guide d'installation](/fr/dev/installation/api)

### Dashboard

> [https://gitlab.com/odeyr/dashboard](https://gitlab.com/odeyr/dashboard)

Le dashboard est une interface web permettant de gérer son serveur local avec facilité. Le guide utilisateur décrit en détails chacune des pages et comment l'utiliser.

Nous avons choisi d'utiliser le cadre logiciel [Vue.js](https://vuejs.org). Ce cadre logiciel est simple, léger et nous permet de développer de nouvelles pages et fonctionnalités rapidement. Plusieurs librairies logicielles sont aussi utilisées, les majeures étant [vue-apollo](https://apollo.vuejs.org/) pour les requêtes vers l'API, [vuex](https://vuex.vuejs.org/) pour la gestion d'états et [vue-router](https://router.vuejs.org/) pour avoir plusieurs pages.

[Guide d'installation](/fr/dev/installation/dashboard)

### Service / Client

> [https://gitlab.com/odeyr/service-dotnet](https://gitlab.com/odeyr/service-dotnet)

Bien que le nom reste à désirer et changera probablement bientôt, le service permet d'automatiser, gérer, relayer les informations des serveurs de jeux jusqu'à l'API et ouvrir au monde le serveur. C'est un client logiciel qui s'installe sur la machine de l'utilisateur. Il s'exécute en arrière plan, ce qui permet un communication transparente entre le dashboard et la machine sur laquelle le service est installé.

Nous avons choisi de développer le service en C# ([.NET Core](https://dotnet.microsoft.com/learn/dotnet/what-is-dotnet)) afin que le service soit multi-plateforme.

[Guide d'installation](/fr/dev/installation/service)
