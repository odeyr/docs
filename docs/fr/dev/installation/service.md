---
title: Installation du Service/Client pour dévelopment
tags:
  {
    service,
    client,
    .net,
    dotnet,
    serveur,
    controle,
    installation,
    dévelopement,
    setup,
  }
lang: fr
---

## Installation du service pour le développement

## Logiciels nécessaires
Le logiciel nécessaire est Visual Studio 2019 Community avec les "Workloads" suivant: .NET desktop development, Universal Windows Platform development et .NET Core cross-platform development.

## Fichier à cloner
Cloner le projet [ici](https://gitlab.com/odeyr/service-dotnet/-/tree/develop)

## Configuration du projet pour le développement
Premièrement vous allez avoir besoin d'un fichier sshtunnel.pem que vous pouvez trouvez dans la dernière version du [Service Odeyr](https://gitlab.com/odeyr/service-dotnet/-/releases). Une fois télécharger ajouter ce fichier au dossieur du projet dans le répertoire suivant (OdeyrWorkerService/)

Ajouter une nouvelle variable d'environnement d'utilisateur avec ODEYR_DATA_LOCATION comme nom de variable. Ensuite, entrez le chemin d'accès dans lequel vous souhaitez que le service installe vos serveurs comme valeur pour la variable d'environnement.
<br/>
<br/>
<img class="image" src="/screenshot-add-environment-variable.png">

À l'intérieur du réportoire que vous avez utilisé comme valeur de variable d'environnement, créé un nouveau fichier nommé "odeyr-global.properties" avec les champs suivant: email, password, authenticated. Remplissez les champs "email" et "password" avec vos identifiants d'authentification Odeyr. Laissez le champs "authenticated" vide. Un champ optionel peut être ajouté à ce fichier, le nom de ce champ optionel est 'endpoint'. La valeur de ce champ permet de choisir à quel endpoint vous souhaitez-vous connecter. Par exemple le champ 'endpoint' pourrait être initialisé à "http://localhost:4000".
[Guide sur la création d'une variable d'environnement](https://www.twilio.com/blog/2017/01/how-to-set-environment-variables.html)
<br/>
<br/>
<img class="image" src="/screenshot-odeyr-global-properties.png">

Redémarrer votre ordinateur pour que les changements aux variables d'environnements prennent effet. Vous êtes maintenat prêt à travailler dans le projet!

