---
title: Installation de l'API pour dévelopment
tags: { api, installation, dévelopement, setup }
lang: fr
---

# Installation pour développement de l'API

::: warning Note aux utilisateurs
Cette article décrit comment installer chacuns des composants de l'application à des fins de dévelopement.

Si vous souhaitez en apprendre plus sur l'installation pour utiliser Odeyr, il vous est conseiller de suivre les intructions de la section du [guide utilisateur](/fr/guide).
:::

## Installation des logiciels nécessaires

1. Installer [docker](https://www.docker.com/) avec [docker-compose](https://docs.docker.com/compose/)
2. Installer [Node.js](https://nodejs.org) et npm
3. Installer [Git](https://git-scm.com/)
4. Cloner le [repo](https://gitlab.com/odeyr/api)
5. (Recommandé) Sélectionner la branche `develop`

## Installation de la base de données

1. Exécuter la commande: `MANAGEMENT_API_SECRET="my-server-secret-123" DATABASE_PASSWORD="prisma" docker-compose -f docker-compose.prisma.yml up -d`

Si tout c'est bien déroulé, en exécutant la commande `docker ps` vous devriez voir ceci:

![Expected docker ps](/screenshot-expected-docker-ps-2020-04-10.png)

::: tip
Il se peut que l'orde ne soit pas exactement la même. Le plus important est que les deux processus fonctionnent et que le port `4466` soit ouvert sur votre réseau locale.
:::

## Configuration du serveur GraphQL

1. Dupliquer le fichier `development.exemple.env`
2. Renommer le fichier dupliqué à `.env` (Aucun nom, juste l'extension)
3. Modifier le contenu du fichier `.env` pour l'ajuster à votre système

### À propos des courriels

L'API utilise Mailgun pour envoyer des courriels transactionnels. Il est nécessaire d'obtenir une clé d'API et un domaine d'envoi valide. Mailgun offre un domaine dit "sandbox" pour les tests. Veuillez vous référer à la documentation de Mailgun pour l'utilisation du domaine d'envoi.

Afin d'éviter d'envoyer des courriels dans le vide ou à des personnes qui ne souhaiteraient pas en recevoir, il est aussi fortement recommandé de modifier la variable d'environnement `REDIRECT_EMAIL_TO` afin que tous les courriels transactionnels soient automatiquement redirigés vers votre adresse courriel sans égard au courriel de l'utilisateur.

## Installation du serveur GraphQL

Il est conseillé de rouler les prochaines commandes dans git bash. Si vous êtes dans powershell, quelques directives suplémentaires pourraient s'appliquer.

1. Exécuter la commande: `npm i -g prisma graphql-cli`
2. Exécuter la commande: `npm i`
3. Exécuter la commande: `prisma deploy`
   1. Si la commande retourne une erreur _"is not numerically signed"_, ouvrir powershell en mode administrateur et exécuter `Set-ExecutionPolicy Unrestricted`, puis réessayer le point 3
4. Exécuter la commande: `prisma generate`
5. (Optionnel) Exécuter la commande: `prisma seed --reset`

Si tout s'est bien déroulé, vous devriez être en mesure de lancer le serveur GraphQL avec la commande: `npm run dev`

### À propos de la connexion à l'API

En accédant au serveur web lancé par l'API, vous devriez être en mesure de commencer à exécuter des requêtes manuellement. La plupart des ressources exposées par l'API requièrent d'être authentifiées. L'API utilise des cookies d'authentification et tente de respecter le principe de _Refresh Token_ et _Access Token_. L'environnement graphique de test (Playground) ne supporte pas les cookies par défaut. Il faut aller dans les paramètres et modifier la ligne: `"request.credentials": "omit"` à `"request.credentials": "include"`
