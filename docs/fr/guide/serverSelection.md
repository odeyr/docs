---
title: Sélection de serveurs
tags: [
  sélection,
  serveurs,
  ajouter,
  télécharger,
  client,
  locale
]
lang: fr
---

# Sélection de serveurs

## Télécharger le client local
<br/>
<img class="image-bigger" :src="$withBase('/screenshot-client-locale-2020-04-10.png')" alt="Client local">

Pour télécharger le client local, cliquez sur « ici ». 

::: tip Tip
Un client local va servir à relayer l'information entre notre base de données et votre infrastructure. Vous devriez installer le client sur la machine sur laquelle vous voulez lancer le serveur. Des exécutables Windowx, Linux et Mac seront à votre disposition. <br></br>

Cliquez ici pour voir la documentation pour [télécharger le client local](https://gitlab.com/odeyr/service-dotnet/-/releases).
:::

À la suite de l'installation du client local, il sera probablement nécessaire de redémarrer l'ordinateur pour appliquer les changements.

## Ajouter un serveur
<p class="step">1. Cliquez sur le bouton ajouter</p>
<br/>
<img class="image-smaller" :src="$withBase('/screenshot-add-server-2020-04-10.png')" alt="Ajouter">

<p class="step">2. Entrez le nom du serveur et cliquez ensuite sur le bouton « Suivant »</p>
<br/>
<img class="image" :src="$withBase('/screenshot-add-server-name-2020-04-10.png')" alt="Nom du serveur">

Choisissez un nom pour votre serveur. Ce nom sera demandé sur l'application Minecraft lorsque vous allez ajouter le serveur au jeu.

<p class="step">3. Sélectionner le type de serveur et cliquez ensuite sur le bouton « Suivant »</p>
<br/>
<img class="image" :src="$withBase('/screenshot-add-server-type-2020-04-10.png')" alt="Type de serveur">

Choisissez la catégorie de serveur qui vous convient. L'option actuellement prise en compte est Vanilla et CraftBukkit. 

::: tip Tip
Vanilla veut dire qu'il s'agit d'une version classique du serveur. Plus tard, une version modé sera aussi disponible pour la sélection.

CraftBukkit est une modification du logiciel serveur de Minecraft (Vanilla Minecraft). Beaucoup plus optimisé pour du déploiement de grande taille que Vanilla. Pour plus d'informations, veuillez consulter https://getbukkit.org/ .
:::

À la suite de l'installation du client local, il sera probablement nécessaire de redémarrer l'ordinateur pour appliquer les changements.

<p class="step">4. Sélectionner la version de Minecraft du serveur et cliquez ensuite sur le bouton « Suivant »</p>
<br/>
<img class="image" :src="$withBase('/screenshot-add-server-version-2020-04-10.png')" alt="Version du serveur">

Choisissez la version de Minecraft du serveur. Certaines versions sont supportées tandis que d'autre ne sont pas supporté sur l'application Odeyr. 

::: tip Tip
Les versions supportées actuellement sont : 1.15.2, 1.12.2 et 1.7.10. D'autres versions seront à venir.
:::

<p class="step">Une confirmation suite à la création du serveur s'affichera :</p>
<img class="image" :src="$withBase('/screenshot-add-server-confirmation-2020-04-10.png')" alt="Confirmation">

Il sera important d'aller préalablement d'aller installer le client local sur votre machine pour procéder à l'installation en locale.
