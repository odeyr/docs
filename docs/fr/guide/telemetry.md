---
title: Télémétrie
tags: [
  télémétrie,
  ping,
  nombre de joueurs,
  joueurs,
  graphique
]
lang: fr
---

# Télémétrie

## Ping

Le graphique de ping permet de savoir en temps réel la connectivité du serveur. 

<img class="icon" :src="$withBase('/screenshot-telemetry-ping-2020-4-20.png')" alt="Ping">

::: tip Tip
Plus elle est haute, plus il y a de latence. Autrement dit, plus le délai de transmission dans les communications est élevés.
:::

## Nombre de joueurs

Le graphique du nombre de joueurs permet de savoir en temps réel le nombre de joueurs qui sont connectés au serveur. Sur l'axe des x du graphique se trouve le nombre de joueurs.

<img class="icon" :src="$withBase('/screenshot-telemetry-players-2020-4-20.png')" alt="Nombre de joueurs">