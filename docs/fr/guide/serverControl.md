---
title: Contrôle du serveur
tags: [
  controle,
  serveur,
  statut,
  console
]
lang: fr
---

# Contrôle du serveur
La page contrôle du serveur permet de gérer le statut du serveur et de voir la console.

## Informations sur le serveur
L'icône à gauche permet de voir le statut du serveur. Si vous placez la souris sur le cercle, le statut sera écrit.

<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Serveur en arrêt"></div>
  <div class="item"><p class="step">Serveur en arrêt</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-running-2020-4-20.png')" alt="Serveur démarrer"></div>
  <div class="item"><p class="step">Serveur démarrer</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-loading-2020-4-20.png')" alt="Serveur en arrêt"></div>
  <div class="item"><p class="step">Serveur en chargement</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Serveur en arrêt"></div>
  <div class="item"><p class="step">Serveur avec une erreur</p></div>
</div>

## Démarrer ou arrêter le serveur
Cliquez sur le bouton pour changer le statut du serveur. 

Si le serveur est démarré, il y aura un bouton « Arrêter » en rouge pour arrêter le serveur.

<img class="image-bigger" :src="$withBase('/screenshot-server-controls-server-running-2020-4-20.png')" alt="Serveur en arrêt">

Si le serveur est arrêté, il y aura un bouton « Démarrer » en vert pour démarrer le serveur.

<img class="image-bigger" :src="$withBase('/screenshot-server-controls-server-start-2020-4-20.png')" alt="Serveur démarrer">

## Console
La console affiche tous les logs sortant du serveur et permet d'avoir un aperçu rapide des commandes qui ont été exécutées. Il sera aussi possible d'envoyer des commandes Minecraft à travers la console du navigateur.

<img class="image-bigger" :src="$withBase('/screenshot-server-controls-console-2020-4-20.png')" alt="Serveur démarrer">