---
title: Profile
tags: [
  profile,
  nom d'utilisateur,
  mot de passe,
  changer mot de passe,
  modifier,
  modification
]
lang: fr
---

# Profile

La page profile contient toutes les informations sur le profile de la personne connectée.

<img class="image" :src="$withBase('/screenshot-profile-form-2020-04-10.png')" alt="Profile">

## Modifier les données
<br>
<img class="image" :src="$withBase('/screenshot-profile-modifier-2020-04-10.png')" alt="Modifier les données">

Pour activer la modification des données, cliquez sur le bouton « Modifier » en bas à droite. 

Il est possible de modifier le nom d'utilisateur, le courriel et le mot de passe. Si vous changez le mot de passe, il faudra confirmer celui-ci dans « Confirmer le mot de passe ». 

Lorsque les modifications sont terminées, cliquez sur le bouton « Soumettre » en vert.