---
title: Installation du service
tags: [
  installation,
  service,
  logiciel,
  cloner,
  commandes
]
lang: fr
---

# Installation du service

## Comment installer le service
<br/>
Voici une courte vidéo pour l'installation du service. 
<br/>
<br/>
<video src="/service-installation-2020-05-11.mp4" width="750" height="450" controls preload></video>

## Logiciels nécessaires
Le logiciel nécessaire est Visual Studio 2019 Community avec les "Workloads" suivant: .NET desktop development, Universal Windows Platform development et .NET Core cross-platform development.

## Fichier à télécharger
Télécharger la dernière version du service [ici](https://gitlab.com/odeyr/service-dotnet/-/releases)

## Comment installer le service (Guide textuelle)
Ajouter une nouvelle variable d'environnement d'utilisateur avec ODEYR_DATA_LOCATION comme nom de variable. Ensuite, entrez le chemin d'accès dans lequel vous souhaitez que le service installe vos serveurs comme valeur pour la variable d'environnement.
[Guide sur la création d'une variable d'environnement](https://www.twilio.com/blog/2017/01/how-to-set-environment-variables.html)
<br/>
<br/>
<img class="image" src="/screenshot-add-environment-variable.png">

À l'intérieur du réportoire que vous avez utilisé comme valeur de variable d'environnement, créé un nouveau fichier nommé "odeyr-global.properties" avec les champs suivant: email, password, authenticated. Remplissez les champs "email" et "password" avec vos identifiants d'authentification Odeyr. Laissez le champs "authenticated" vide.
<br/>
<br/>
<img class="image" src="/screenshot-odeyr-global-properties.png">

Redémarrer votre ordinateur pour que les changements fait aux variables d'environnement prennent effets. Et c'est tout!

