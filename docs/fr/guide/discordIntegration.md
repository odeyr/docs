---
title: Intégration Discord
tags: [
  intégration,
  discord,
  partager,
  supprimer
]
lang: fr
---

# Intégration Discord

La page d'intégration discord permet de gérer le bot dans un serveur discord.

# Options

<p class="step">Salon</p>
<img class="image-smaller" :src="$withBase('/screenshot-discord-integration-channel-2020-04-10.png')" alt="Channel">

Sélectionnez le channel que vous voulez utiliser pour le bot Discord. 

<p class="step">Message épinglé</p>
<img class="image-smaller" :src="$withBase('/screenshot-discord-integration-pin-message-2020-04-10.png')" alt="Pin message">

Sélectionnez si oui ou non vous voulez le pin message.

::: tip Tip
Un pin message est affiché à gauche en haut de l'écran du jeu et est visible pour tout les joueurs du jeu pendant un certain nombre de temps.
:::

<p class="step">Historique</p>
<img class="image-smaller" :src="$withBase('/screenshot-discord-integration-historique-2020-04-10.png')" alt="Historique">

Sélectionnez si oui ou non vous voulez garder l'historique.

# Supprimer l'intégration discord
<br>
<img :src="$withBase('/screenshot-discord-integration-supprimer-integration-2020-04-10.png')" alt="Supprimer l'intégration">

Vous pouvez supprimer l'intégration Discord en cliquant sur le bouton « Supprimer l'intégration » en rouge.
 
::: tip Tip
Il sera possible de réactiver l'intégration à tout moment.
:::

