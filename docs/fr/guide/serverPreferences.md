---
title: Préférences serveur
tags: [
  préférences,
  serveur,
  avancé,
  options
]
lang: fr
---

# Préférences serveur

La page préférences serveur permet de modifier les options du serveur.

## Options normales

<p class="step">Difficulté</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les difficultés : [Difficulté](https://minecraft-fr.gamepedia.com/Difficult%C3%A9)

<p class="step">Mode de jeu</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les modes de jeu : [Mode de jeu](https://minecraft-fr.gamepedia.com/Gameplay#Modes_de_jeu)

<p class="step">Hauteur du monde maximum</p>

Entrez un nombre supérieur à 0 pour la hauteur du monde maximum en inscrivant le chiffre.

<p class="step">Mot de bienvenue</p>

Entrez le mot de bienvenue que tous les joueurs du serveur verront sur le jeu Minecraft.

<p class="step">Définir le pack de ressources</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les packs de ressources : [Pack de ressources](https://minecraft-fr.gamepedia.com/Pack_de_ressources)

<p class="step">Distance de vision</p>

Entrez un nombre entre 1 et 32 pour la distance de vision.

<p class="step">Forcer le mode de jeu</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les modes de jeu : [Mode de jeu](https://minecraft-fr.gamepedia.com/Gameplay#Modes_de_jeu)

<p class="step">Mode sans échec</p>

Sélectionnez si vous voulez oui ou non un mode sans échec.

<p class="step">Nombre de joueurs maximum</p>

Entrez un nombre supérieur à 0 pour le nombre de joueurs maximum.

<p class="step">Permettre le PVP</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur le PVP : [PvP](https://minecraft-fr.gamepedia.com/Tutoriels/PvP)

## Options avancées
<br>
<img :src="$withBase('/screenshot-serveur-preferences-avancee-2020-04-10.png')" alt="Options avancées">

Cliquez sur la barre « Options avancées » en bas de la page pour voir les options avancées.

<p class="step">Permettre de voler</p>

Sélectionner si vous voulez oui ou non que les joueurs puissent voler.

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur le vol : [Vol](https://minecraft-fr.gamepedia.com/Vol)

<p class="step">Permettre l'enfer</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur l'enfer : [Nether](https://minecraft-fr.gamepedia.com/Le_Nether)

<p class="step">Permettre les blocs de commandes</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les blocs de commandes : [Bloc de commande](https://minecraft-fr.gamepedia.com/Bloc_de_commande)

<p class="step">Utiliser GameSpy4</p>

Le GameSpy4 est utilisé pour obtenir des informations du serveur.

<p class="step">Définir le port de GameSpy4</p>

Entrez un nombre supérieur à 1 pour le définir le port de GameSpy4.

<p class="step">Utiliser l'accès à distance</p>

L'accès à distance correspond à la connexion au serveur à distance.

<p class="step">Définir le port pour l'accès à distance</p>

Entrez un nombre supérieur à 1 pour définir le port pour l'accès à distance.

<p class="step">Niveau de permission de fonctions</p>

Entrez un nombre entre 1 et 4 pour le niveau de permission de fonctions.

<p class="step">Génère des structures</p>

Sélectionnez si vous voulez oui ou non générer des structures.

<p class="step">Nom du monde</p>

Entrez le nom du monde.

<p class="step">Seed du monde</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les seed : [Génération de carte](https://minecraft-fr.gamepedia.com/Graine_(g%C3%A9n%C3%A9ration_de_carte))

<p class="step">Type du monde</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les types de monde : [Type de monde](https://minecraft-fr.gamepedia.com/Type_de_monde)

<p class="step">Nombre de tick maximum</p>

Entrez un nombre supérieur à 0 pour le nombre de tick maximum.
Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les ticks : [Tick](https://minecraft-fr.gamepedia.com/Tick)

<p class="step">Grosseur du monde maximum</p>

Entrez un nombre supérieur à 0 pour la grosseur du monde maximum.

<p class="step">Taille de compression des paquets</p>

Entrez un nombre supérieur à -1 pour la taille de compression des paquets.
Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les paquets : [Protocole de paquets](https://minecraft-fr.gamepedia.com/Protocole_de_serveur_Classique#Protocole_de_paquets)

<p class="step">Vérification des comptes Mojang</p>

Sélectionnez si vous voulez oui ou non la vérification des comptes Mojang.

<p class="step">Permission par défaut de l'OP</p>

Sélectionnez le niveau par défaut de l'opérateur.

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur l'opérateur : [Opérateur](https://minecraft-fr.gamepedia.com/Op%C3%A9rateur)

<p class="step">Temps AFK avant déconnexion</p>

Entrez un nombre supérieur à 0 pour le temps AFK avant déconnexion. 

<p class="step">Le port du serveur</p>

Entrez un nombre supérieur à 1 pour le port du serveur.
Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les serveurs : [Serveur](https://minecraft-fr.gamepedia.com/Serveur)

<p class="step">L'adresse IP du serveur</p>

Entrez l'adresse IP du serveur.

<p class="step">Envoi de données snoop</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les snoop: [Propriétées du serveur](https://minecraft-fr.gamepedia.com/Server.properties)

<p class="step">Apparition d'animaux</p>

Sélectionnez si vous voulez oui ou non vous souhaiter l'apparition d'animaux.

<p class="step">Apparitions de personnages non joueurs (PNJ)</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les personnages qui ne sont pas des joueurs : [Personnages non joueurs](https://minecraft-fr.gamepedia.com/PNJ)

<p class="step">Apparition de monstres</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur les créatures : [Créatures](https://minecraft-fr.gamepedia.com/Cr%C3%A9atures)

<p class="step">Optimisation pour serveur Linux</p>

Sélectionnez si vous voulez oui ou non vous souhaiter l'optimisation pour serveur Linux.

<p class="step">Grosseur de la zone de protection</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur la zone de protection : [Propriétées du serveur](https://minecraft-fr.gamepedia.com/Server.properties)

<p class="step">Obliger l'utilisation de la « White list »</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur la « White list » : [Propriétées du serveur](https://minecraft-fr.gamepedia.com/Server.properties)

<p class="step">Utiliser la « White list »</p>

Référez-vous au wiki de Minecraft en cliquant sur ce lien pour en apprendre plus sur la « White list » : [Propriétées du serveur](https://minecraft-fr.gamepedia.com/Server.properties)
