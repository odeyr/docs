---
title: Édition du serveur
tags: [
  modifier,
  éditer,
  serveur,
  options
]
lang: fr
---

# Édition du serveur

## Éditer les données

Le graphique du nombre de joueurs permet de savoir en temps réel le nombre de joueurs qui sont connectés au serveur. Sur l'axe des x du graphique se trouve le nombre de joueurs.

<p class="step">Éditer le nom du serveur</p>
<img class="icon" :src="$withBase('/screenshot-edit-server-edit-2020-4-20.png')" alt="Éditer un serveur">
<br>
<p class="step">1. Cliquez sur le crayon dans le bouton gris</p>
<img class="delete-icon" :src="$withBase('/screenshot-edit-server-create-bouton-2020-4-20.png')" alt="Éditer le nom du serveur">

<p class="step">2. Entrez le nouveau nom du serveur</p>

<p class="step">3. Cliquez sur la disquette dans le bouton vert</p>
<img class="delete-icon" :src="$withBase('/screenshot-edit-server-save-bouton-2020-4-20.png')" alt="Enregistrer les modifications">

<p class="step">4. Attendez que ça finisse de charger</p>
<img class="delete-icon" :src="$withBase('/screenshot-edit-server-loading-bouton-2020-4-20.png')" alt="Loading">

## Supprimer le serveur
<br>
<img class="icon" :src="$withBase('/screenshot-edit-server-remove-server-2020-4-20.png')" alt="Supprimer le serveur">
<br>

Vous pouvez supprimer le serveur en cliquant sur le bouton « Supprimer le serveur » en rouge. Une confirmation sera demandé avant de supprimer le serveur.
 
::: danger Attention
Il ne sera pas possible de réactiver le serveur. Vous perdrez tout.
:::


