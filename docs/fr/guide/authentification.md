---
title: Authentification
tags: [
  authentification,
  connexion,
  connection,
  mot de passe,
  s'inscrire,
  inscription
]
lang: fr
---

# Authentification

## Connexion
<br/>
<img class="image" :src="$withBase('/screenshot-connexion-2020-04-10.png')" alt="Connexion">

La page de connexion permet de s'identifier avec le courriel et le mot de passe entré lors de l'inscription.

## Inscription
<br/>
<img class="image" :src="$withBase('/screenshot-register-2020-04-10.png')" alt="Inscription">

La page d'inscription permet de s'inscrire avec un nom d'utilisateur, un courriel et un mot de passe.

## Mot de passe oublié
<br/>
<img class="image" :src="$withBase('/screenshot-forgot-password-2020-04-10.png')" alt="Mot de passe oublié">

La page de mot de passe oublié permet de réinitialiser le mot de passe. Un courriel sera envoyé dans un cours délais, afin d'obtenir le lien pour réinitialiser le mot de passe. 

<p class="step">Réception du courriel</p>

Une fois dans votre messagerie, cliquez sur le bouton « Réinitialiser le mot de passe ». Si le bouton ne fonctionne pas, cliquez sur le lien. Vous serez ensuite redirigé vers le site afin de réinitialiser votre mot de passe. Vous serez ensuite capable de vous reconnecter avec votre nouveau mot de passe.
