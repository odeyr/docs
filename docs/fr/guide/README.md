---
title: Introduction
tags: [
  projet,
  présentation,
  début,
  commencement,
  depart,
  intro
]
lang: fr
---

::: warning Documentation en construction
:construction:
La documentation est en cours de rédaction, il se peut que les informations ne soit pas 100% à jour et change régulièrement, que certaine page ne comporte pas toute l'information et qu'il y ait quelques erreurs de frappes ou faute de français.
:::

# Introduction

Le Projet Odeyr est une initiative afin de simplifier l'installation, le partage et l'accès à vos serveurs de jeux personnels. Ce que l'on entend par personnel, c'est un serveur qui s'exécute sur votre propre ordinateur et utilise donc les ressources de votre ordinateur.

Traditionnellement, il existe deux moyens pour avoir un serveur de jeux pour jouer avec ses amis. Le premier requiert de payer un hébergeur spécialisé, ce qui peut être couteux. Le deuxième est qu'ODEYR rend le hosting à la maison facile et rapide à configurer.

## Comment cela fonctionne

::: tip Fonctionnement technique
Le prochain paragraphe ne s'adresse pas à l'approche technique du projet, mais bien aux principes de base qui régissent l'application du point de vue d'un utilisateur.

Si vous êtes à la recherche de la documentation technique, rendez-vous dans la [section de documentation développeur](/dev).
:::
