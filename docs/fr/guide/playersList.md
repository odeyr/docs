---
title: Liste des joueurs
tags: [
  liste,
  joueurs,
  operateur,
  bannir
  expulser
]
lang: fr
---

# Liste des joueurs
La page liste des joueurs contient la liste des joueurs qui se sont connectés au moins une fois au serveur. 

<p class="step">Carte du joueur</p>
<img :src="$withBase('/screenshot-players-list-card-2020-04-10.png')" alt="Carte du joueur">

La photo du compte du joueur, le nom d'utilisateur du joueur et les boutons d'actions sont affichés.

<p class="step">Opérateur</p>
<img class="icon" :src="$withBase('/screenshot-players-list-verified-2020-04-10.png')" alt="Opérateur">

Cliquez sur l'icône pour rendre un utilisateur opérateur.

::: tip Tip
Un utilisateur opérateur est un utilisateur qui a des droits que certains utilisateurs n'ont pas. Autrement dit, il est administrateur.
:::

<p class="step">Bannir</p>
<img class="icon" :src="$withBase('/screenshot-players-list-block-2020-04-10.png')" alt="Bloquer">

Cliquez sur l'icône pour bannir un utilisateur.

::: tip Tip
Un utilisateur banni est un utilisateur qui ne peut plus accéder au serveur.
:::

<p class="step">Expulser</p>
<img class="icon" :src="$withBase('/screenshot-players-list-warning-2020-04-10.png')" alt="Expulser">

Cliquez sur l'icône pour expulser un utilisateur.

::: tip Tip
Un utilisateur expulsé est un utilisateur qui sera rejeté de la partie actuelle, mais qui pourra se reconnecter pour rentrer dans nouveau dans le serveur.
:::