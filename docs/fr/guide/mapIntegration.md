---
title: Map intégration
tags: [
  map,
  intégration,
]
lang: fr
---

# Map intégration

La map permet de voir la carte du jeu. On peut aussi y voir les joueurs qui sont au même moment entrain de jouer. 

Pour voir la page, vous devez avoir un type de serveur « CraftBukkit ».

<img class="icon" :src="$withBase('/screenshot-map-integration-2020-05-11.png')" alt="Map intégration">

Il y a aussi une map en 3 dimensions.

<img class="icon" :src="$withBase('/screenshot-map-3d-integration-2020-05-11.png')" alt="3D map intégration">
