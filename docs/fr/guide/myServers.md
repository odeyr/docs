---
title: Mes serveurs
tags: [
  serveurs,
  statut,
  modifier,
  éditer,
  supprimer,
  installer
]
lang: fr
---

# Mes serveurs

La page mes serveurs se situe dans le menu déroulant en haut à droite de la fenêtre de navigation. Elle permet de supprimer, de modifier et d'installer un serveur.

## Statut du serveur

Le statut est affiché sous forme d'icône qui change de couleur en fonction du statut du serveur comme dans la page [Contrôle de serveur](/guide/serverControl).

<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Serveur en arrêt"></div>
  <div class="item"><p class="step">Serveur en arrêt</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-running-2020-4-20.png')" alt="Serveur démarrer"></div>
  <div class="item"><p class="step">Serveur démarré</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-loading-2020-4-20.png')" alt="Serveur en arrêt"></div>
  <div class="item"><p class="step">Serveur en chargement</p></div>
</div>
<div class="flex">
  <div class="item"><img class="status" :src="$withBase('/screenshot-server-controls-status-stopped-2020-4-20.png')" alt="Serveur en arrêt"></div>
  <div class="item"><p class="step">Serveur arrêter avec une erreur</p></div>
</div>

## Options du serveur
<br>
<img :src="$withBase('/screenshot-my-servers-menu-2020-04-10.png')" alt="Menu">

Pour afficher les options du serveur, cliquez sur l'icône avec les trois points verticaux.

### Modifier
Pour modifier le serveur, cliquez sur « Modifier », ce qui mènera à la page [édition du serveur](/guide/editServer).

### Supprimer
Pour supprimer le serveur, cliquez sur « Supprimer ».

::: danger Supprimer le serveur
 Il ne sera plus possible de remettre le même serveur une fois supprimé. Toutes les données du serveur seront supprimées.
:::