# Odeyr Documentation

La documentation d'Odeyr permet à tous les développeurs de partager le même savoir sur le projet ainsi que de servir de guide pour les nouveaux utilisateurs. Une section de la documentation est réservé au guide utilisateur, expliquant en détails les pages et le fonctionnement de celles-ci.

La documentation est conçu avec le générateur de site static [VuePress](https://vuepress.vuejs.org)

## Étapes d'installation pour développement

### Installation des logiciels nécessaire

1. Installer [Node.js](https://nodejs.org) et npm
2. Installer [Git](https://git-scm.com/)
3. Cloner le repo

### Installation de la documentation

1. Exécuter la commande: `npm i`

Vous devriez maintenant être en mesure de lancer le serveur de développement de la documentation avec la commande: `npm start`
